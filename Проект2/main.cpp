#define _CRT_SECURE_NO_WARNINGS
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include <fstream>
#include <windows.h>
#include <time.h>
#include <tchar.h>
#include <vector>
#include <utility>

using namespace sf;
using namespace std;

#define mouse_left_pressed (event.type == event.MouseButtonPressed && event.mouseButton.button == Mouse::Left)
#define mouse_right_pressed (event.type == event.MouseButtonPressed && event.mouseButton.button == Mouse::Right)
#define mouse_middle_pressed (event.type == event.MouseButtonPressed && event.mouseButton.button == Mouse::Middle)
#define mouse_left_released (event.type == event.MouseButtonReleased && event.mouseButton.button == Mouse::Left)
#define mouse_right_released (event.type == event.MouseButtonReleased && event.mouseButton.button == Mouse::Right)
#define mouse_middle_released (event.type == event.MouseButtonReleased && event.mouseButton.button == Mouse::Middle)

//////////////////////////////////
//	      ������� ������        //
#define c_background Color(245, 245, 245)
#define c_select2 Color(137, 180, 255)
#define c_grayLL Color(236, 232, 236)
#define c_grayL Color(218, 211, 218)
#define c_gray Color(185, 175, 185)
#define c_text Color(80, 80, 85)
#define c_alpha Color(0, 0, 0, 0)
#define c_green Color(85, 170, 0)
#define c_red Color(250, 80, 0)
#define c_blue Color(100, 144, 187)
#define c_yellow Color(254, 250, 129)
#define c_menu Color(190, 192, 194)
#define c_menuR Color(220, 136, 97)
#define c_menuG Color(137, 181, 97)
#define c_menuB Color(143, 143, 203)
#define c_agrayD Color(145, 135, 145, 190)
#define c_abackground Color(245, 245, 245, 128)

//////////////////////////////////
//	   ���������� ����������    //
RenderWindow window; // �������� ��������
Event event; // ���� �������
bool mouse_left = false, mouse_right = false, mouse_middle = false;
//bool smooth = false;
int mode = 0;
int window_width = 0, window_height = 0;
int mouse_x = 0, mouse_y = 0;
int mouse_x_prev = 0, mouse_y_prev = 50;
int view_x = 0, view_y = 0, vm_x = 0, vm_y = 0;
int view_bordier_left = 0, view_bordier_right = 0, view_bordier_top = 0, view_bordier_bottom = 0;
int nodes_count = 0;
int team_count[5] = { 0, 0, 0, 0, 0 };
int activate_id_from = -1, activate_id_to = -1;
int room = 0;
int mousedelta = 0;
int add_unit[5];
int step = 0;
bool nextstep = true;
const double PI = 3.1415926535897932384626433832795;
const double PI2 = 1.5707963267948966192313216916398;
bool sound_on = false;
bool music_on = false;
Music snd_click;
Music snd_click2;
Music snd_back_menu;
Music snd_back_game;
RenderTexture sub_window;

//////////////////////////////////
//	      �������� �������      //
int sign(int a)
{
	if (a == 0) return 0;
	if (a < 0) return -1;
	else return 1;
}
int min(int a, int b)
{
	return (a<b) ? a : b;
}
int max(int a, int b)
{
	return (a>b) ? a : b;
}
double asqrt(double a)
{
	return sign(a)*sqrt(abs(a));
}

//////////////////////////////////
//	  �������������� �������    //
double point_directionG(double arg0, double arg1, double arg2, double arg3)
{
	double xx = arg0 - arg2;
	double yy = arg1 - arg3;
	double dir = atan2(yy, xx) - 3.1415;
	return (dir * 180 / 3.1415);
}
double point_directionR(double arg0, double arg1, double arg2, double arg3)
{
	double xx = arg0 - arg2;
	double yy = arg1 - arg3;
	double dir = atan2(yy, xx) - 3.1415;
	return (dir);
}
double point_distance(double arg0, double arg1, double arg2, double arg3)
{
	double xx = arg0 - arg2;
	double yy = arg1 - arg3;
	return (int)(sqrt(fabs(xx*xx - yy*yy)));
}

//////////////////////////////////
//	     ������� ���������      //
// �������������
RectangleShape rectangle_fun;
template <class �emp> void draw_rectangle(int x, int y, int w, int h, Color c, �emp &ren)
{
	rectangle_fun.setOutlineThickness(0);
	rectangle_fun.setFillColor(c);
	rectangle_fun.setPosition(x, y);
	rectangle_fun.setSize(Vector2f(w, h));
	ren.draw(rectangle_fun);
}

// ����
CircleShape circle_fun;
template <class �emp> void draw_circle(int x, int y, int r, Color c, �emp &ren)
{
	circle_fun.setOutlineThickness(0);
	circle_fun.setFillColor(c);
	circle_fun.setPosition(x - r, y - r);
	circle_fun.setRadius(r);
	ren.draw(circle_fun);
}
template <class �emp> void draw_circleOut(int x, int y, int r, int rim, Color c, �emp &ren)
{
	circle_fun.setOutlineThickness(rim);
	circle_fun.setFillColor(c_alpha);
	circle_fun.setOutlineColor(c);
	circle_fun.setPosition(x - r + rim, y - r + rim);
	circle_fun.setRadius(r - rim);
	ren.draw(circle_fun);
}

// �����
template<class �emp> void draw_lineW(int x1, int y1, int x2, int y2, int w, int h1, int h2, Color c, �emp &ren)
{
	double dir = point_directionR(x1, y1, x2, y2);
	float wir = (float)w * 0.5;
	float n1x, n1y, n2x, n2y;
	n1x = x1 + h1*cos(dir);
	n1y = y1 + h1*sin(dir);
	n2x = x2 + h2*cos(dir + PI);
	n2y = y2 + h2*sin(dir + PI);
	Vertex vertices[4] =
	{
		Vertex(Vector2f(n1x + wir*(float)cos(dir + PI2), n1y + wir*(float)sin(dir + PI2))),
		Vertex(Vector2f(n1x + wir*(float)cos(dir - PI2), n1y + wir*(float)sin(dir - PI2))),
		Vertex(Vector2f(n2x + wir*(float)cos(dir - PI2), n2y + wir*(float)sin(dir - PI2))),
		Vertex(Vector2f(n2x + wir*(float)cos(dir + PI2), n2y + wir*(float)sin(dir + PI2))),
	};
	vertices[0].color = vertices[1].color = vertices[2].color = vertices[3].color = c;
	ren.draw(vertices, 4, Quads);
}

// �����
string ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.!?\"\',=+-*/\\_()[]<>@%^:&$;~|{}#�����Ũ���������������������������������������������������������� �";
Texture texture_font;
Sprite snum;
String int_to_string(int a, int k)
{
	String str = "";
	if (a == 0)str = "0";
	while (a != 0)
	{
		str = (char)('0' + (a % 10)) + str;
		a /= 10;
	}
	while (str.getSize() < k) str = '0' + str;
	return str;
}
template<class �emp> void draw_symbol(char ch, int x, int y, �emp &ren)
{
	snum.setTextureRect(IntRect(9 * (ALPHABET.find(ch, 0)), 0, 9, 15));
	snum.setPosition(Vector2f(x, y));
	ren.draw(snum);
};
template<class �emp> void draw_text(int x, int y, String str, int charW, Color c, �emp &ren)
{
	snum.setColor(c);
	for (int i = 0; i < str.getSize(); i++)
		draw_symbol(str[i], x + i * charW, y, ren);
}
template<class �emp> void draw_symbol_or(char ch, int x, int y, Color c1, Color c2, �emp &ren)
{
	snum.setColor(c2);
	snum.setTextureRect(IntRect(9 * (ALPHABET.find(ch, 0)), 0, 9, 15));

	snum.setPosition(Vector2f(x + 1, y)); ren.draw(snum);
	snum.setPosition(Vector2f(x, y + 1)); ren.draw(snum);
	snum.setPosition(Vector2f(x - 1, y)); ren.draw(snum);
	snum.setPosition(Vector2f(x, y - 1)); ren.draw(snum);

	snum.setPosition(Vector2f(x + 1, y + 1)); ren.draw(snum);
	snum.setPosition(Vector2f(x + 1, y - 1)); ren.draw(snum);
	snum.setPosition(Vector2f(x - 1, y + 1)); ren.draw(snum);
	snum.setPosition(Vector2f(x - 1, y - 1)); ren.draw(snum);

	snum.setColor(c1);
	snum.setPosition(Vector2f(x, y)); ren.draw(snum);
};
template<class �emp> void draw_text_or(int x, int y, String str, int charW, Color c1, Color c2, �emp &ren)
{
	for (int i = 0; i < str.getSize(); i++)
		draw_symbol_or(str[i], x + i * charW, y, c1, c2, ren);
}

//////////////////////////////////
//	            �����           //
int matrix[1000][1000]; // ������� ���������
bool connect_get(int a, int b)
{
	if ((matrix[a][b] == 1) || (matrix[b][a] == 1)) return true;
	return false;
}

//////////////////////////////////
//	          �������           //
class Label
{
	int offsetX = 0;
	int charW = 9;
	RectangleShape rect;
public:
	RenderTexture holst;
	String text = "label";
	int left, top, height, width;
	int al = 0;

	Label(String txt, int al1, int xx, int yy, int ww, int hh)
	{
		left = xx;
		top = yy;
		width = max(ww, charW*txt.getSize());
		height = hh;
		text = txt;
		al = al1;
	}

	void setPosition(int xx, int yy)
	{
		left = xx;
		top = yy;
	}

	void Draw()
	{
		if (al == 0) offsetX = 0;
		if (al == 1) offsetX = width / 2 - charW * text.getSize() / 2;
		if (al == 2) offsetX = width - charW * text.getSize();

		draw_text(left + offsetX, top, text, 9, c_text, window);
	}

};

class ListBox
{
	Color col;
public:
	int offsetY;
	int offsetYto; // ��� ������� ���������
	RenderTexture holst;
	int left, top, height, width;
	int selectIndex;
	int sizeOf; // ���-�� ��������� � ������
	int item_height = 17;
	String mass[512]; // ������ ���������

	ListBox(int l, int t, int w, int h)
	{
		left = l;
		top = t;
		height = h;
		width = w;
		selectIndex = 0;
		sizeOf = 0;
		holst.create(width, height);
		holst.setSmooth(false);
	}

	void setPosition(int xx, int yy)
	{
		left = xx;
		top = yy;
	}

	void setSize(int xx, int yy)
	{
		if (width != xx || height != yy)
		{
			height = yy;
			width = xx;
			holst.create(width, height);
		}
	}

	void ItemAdd(string s)
	{
		mass[sizeOf] = s;
		sizeOf++;
	}

	void ItemDel(int id)
	{
		mass[id] = "";
		if (id != sizeOf - 1)
		for (int i = id; i < sizeOf - 1; i++)
			mass[i] = mass[i + 1];

		sizeOf--;
	}

	void Clear()
	{
		offsetY = 0;
		for (int i = 0; i < sizeOf; i++)
			mass[i] = "";
		sizeOf = 0;
		selectIndex = 0;
	}

	void Update()
	{
		if (mouse_x > left && mouse_x < left + width && mouse_y > top && mouse_y < top + height)
		{
			if (mouse_left_pressed)
				selectIndex = min(((offsetY + mouse_y - top) / item_height), sizeOf - 1);
		}
	}

	void Draw()
	{
		if (abs(offsetYto - offsetY) < 640)
			offsetY += sign(offsetYto - offsetY) * max(1, sqrt(abs(offsetYto - offsetY)));
		else offsetY = (offsetYto + offsetY) / 2;

		holst.clear(c_alpha);
		for (int i = 0; i < sizeOf; i++)
		{
			col = c_alpha;
			//if ((mouse_x > left) && (mouse_x < left + width) && (mouse_y - top >= 4 + item_height*i - offsetY) && (mouse_y - top < 4 + item_height*i - offsetY + item_height))
			//	col = Color(20, 20, 80, 50);

			if (selectIndex == i) col = c_select2;

			draw_rectangle(0, item_height*i - offsetY, width, item_height, col, holst);
			draw_text(10, item_height * i - offsetY + 1, mass[i], 9, c_text, holst);
		}

		holst.display();
		Sprite ss;
		ss = Sprite(holst.getTexture());
		ss.setPosition(left, top);
		window.draw(ss);
	}
};

class VScroll
{
public:
	int left, top, height, width, rad;
	int Up, Heig, OffY;
	bool press = false;

	VScroll(int l, int t, int w, int h)
	{
		left = l;
		top = t;
		width = w;
		height = h;
		Up = 0;
		Heig = h / 2;
		rad = w / 2;
		OffY = 0;
	}

	void setPosition(int xx, int yy)
	{
		left = xx;
		top = yy;
	}

	void setSize(int xx, int yy)
	{
		height = yy;
		width = xx;
		rad = width / 2;
	}

	void Update(ListBox &lb) // EVENT
	{
		if (mouse_left_pressed && !press)
		if (mouse_x > left - 4 && mouse_x<left + rad * 2 + 4 && mouse_y>top + Up && mouse_y < top + Up + Heig)
		{
			press = true;
			OffY = mouse_y - Up;
		}

		if (!mouse_left) press = false;
	}

	void Draw(ListBox &lb)
	{
		if (press)
		{
			Up = mouse_y - OffY;
			//cout << Up << endl;
			if (Up < 0) Up = 0;
			if (Up + Heig > height) Up = height - Heig;
			if ((height != Heig)) lb.offsetYto = ((lb.item_height*lb.sizeOf - lb.height) * Up) / (height - Heig);
			else lb.offsetYto = 0;
		}

		if (!press && mouse_y > top && mouse_y < top + height)
		{
			if (mousedelta != 0)
			if (mousedelta > 0)
			{
				Up -= 5;
				if (Up < 0) Up = 0;
			}
			else
			{
				Up += 5;
				if (Up + Heig > height) Up = height - Heig;
			}
			if (height != Heig) lb.offsetYto = ((lb.item_height*lb.sizeOf - lb.height) * Up) / (height - Heig);
		}

		draw_circle(left + rad, top + rad, rad, c_gray, window);
		draw_circle(left + rad, top + height - rad, rad, c_gray, window);
		draw_rectangle(left, top + rad, width, height - rad - rad, c_gray, window);

		draw_circle(left + rad, top + Up + rad, rad, c_grayL, window);
		draw_circle(left + rad, top + Up + Heig - rad, rad, c_grayL, window);
		draw_rectangle(left, top + Up + rad, width, Heig - rad - rad, c_grayL, window);
	}
};

class Button
{
public:
	Color colto;
	Color col;
	int left, top, height, width;
	bool press = false;
	String text = "";
	int sdvigX = 0;
	int sdvigY = 0;
	int charW = 9;
	double addr = 0.0;

	Button(string txt, int l, int t, Color c1)
	{
		text = txt;
		left = l;
		top = t;
		width = 80;
		height = 80;
		sdvigX = (width - text.getSize() * 9) / 2;
		sdvigY = (height - 15) / 2;
		colto = c1;
	}

	void setPosition(int xx, int yy)
	{
		left = xx;
		top = yy;
	}

	bool mouse_enter()
	{
		if (mouse_x + 40 > left && mouse_y + 40 > top && mouse_x + 40 < left + width && mouse_y + 40 < top + height) return true;
		return false;
	}

	void Update()
	{
		press = false;
		if (mouse_left_released && mouse_enter()){
			press = true; addr = 0.0;
			if (sound_on) snd_click.play();
		}
	}

	void Draw(float timer)
	{
		timer /= 14500.0;
		col = c_abackground;
		if (mouse_enter())
		{
			col = colto;
			if (addr < 7.0) addr += sqrt(7.0 - addr) / 4.0*timer;
			if (addr > 7.0) addr = 7.0;
		}
		else
		{
			if (addr > 0.0) addr -= sqrt(addr) / 4.0*timer;
			if (addr < 1.1) addr = 0.0;
		}
		draw_circle(left, top, width / 2 - addr, col, window);
		draw_circleOut(left, top, width / 2 + addr, 2, col, window);
		draw_text(left + sdvigX - 40, top + sdvigY - 40, text, charW, c_text, window);
	}
};

class CheckBox
{
public:
	Color colto;
	Color col;
	int left, top, height, width;
	bool check = false;
	String text = "";
	int charW = 9;
	double rad = 0.0;

	CheckBox(string txt, int l, int t, int w, int h)
	{
		text = txt;
		left = l;
		top = t;
		width = w;
		height = 16;
	}

	void setPosition(int xx, int yy)
	{
		left = xx;
		top = yy;
	}

	bool mouse_enter()
	{
		if (mouse_x > left && mouse_y > top && mouse_x < left + width && mouse_y < top + height) return true;
		return false;
	}

	void Update()
	{
		if (mouse_left_pressed && mouse_enter())
			check = !check;
	}

	void Draw(float timer)
	{
		timer /= 14500.0;
		if (check)
		{
			if (rad < 8.0) rad += sqrt(8.0 - rad) / 4.0*timer;
		}
		else
		{
			if (rad > 0.0) rad -= sqrt(rad) / 4.0*timer;
		}


		draw_circle(left + 8, top + 8, 8 - rad, c_text, window);
		draw_circleOut(left + 8, top + 8, rad, 2, c_text, window);
		draw_text(left + 20, top, text, charW, c_text, window);
	}
};

class Node
{
public:
	int id; // ������ �������������
	int units; // ���-�� ������ ������
	int x, y;
	double r; // ������� �� �����
	int start_x = 0, start_y = 0, x_to = 0, y_to = 0;
	int team; // ����������� �������
	Color col = c_gray;
	CircleShape circle_sel; int dir_sel = 0; // �������� ������
	RectangleShape rect_sel; // �������
	double jrad = 27.0;
	double angle = 0.0;
	int send = 0;

	Node()
	{
		circle_sel.setFillColor(c_text);
		circle_sel.setOrigin(2, 2);
		circle_sel.setRadius(2);
		units = 0;
		team = 0;
		id = 0;
		x = 0;
		y = 0;
		r = 40;
		start_x = x;
		start_y = y;
		rect_sel.setFillColor(c_green);
	}

	void Set(int a, int b)
	{
		x = a;
		y = b;
		start_x = a;
		start_y = b;
	}

	void Clean()
	{
		units = 0;
		team = 0;
	}

	bool mouse_enter()
	{
		if (pow(mouse_x - (x + view_x), 2) + pow(mouse_y - (y + view_y), 2) < pow(r + 4, 2)) return true;
		return false;
	}

	void EVENT(float time, Node Nodes[])
	{
		if (step == 0 && mouse_enter() && add_unit[1]>0 && team == 1)
		{
			if (mouse_left_pressed) { units++; add_unit[1]--; }
			if (event.type == Event::KeyPressed && (event.key.code == 58 || event.key.code == Keyboard::Space)) { units += add_unit[1]; add_unit[1] = 0; }
		}

		if (step == 1) // attack
		{
			if (mouse_left_pressed && mouse_enter() && units > 1 && team == 1)
			if (activate_id_from != id) { activate_id_from = id; send = units - 1; }
			else activate_id_from = -1;

			if (mouse_left_pressed && mouse_enter() && activate_id_from != -1 && connect_get(activate_id_from, id) && team != 1)
				activate_id_to = id;
		}

		if (step == 2 && nextstep) // move
		{
			if (mouse_left_pressed && mouse_enter() && team == 1)
			{
				if (activate_id_from == -1) { if (units > 1) { activate_id_from = id; send = units - 1; } }
				else 
				{
					if (activate_id_from == id) 
						activate_id_from = -1;
					else  
						activate_id_to = id; 
				}
			}
		}

		if (activate_id_from == id && step > 0)
		{
			if (units < 2)activate_id_from = -1;
			send += mousedelta;

			if ((event.type == Event::KeyPressed && event.key.code == Keyboard::Add)) send++;
			if ((event.type == Event::KeyPressed && event.key.code == Keyboard::Subtract)) send--;

			send = max(1, min(units - 1, send));
		}
		if (mouse_left_pressed && mouse_enter() && sound_on)
			snd_click2.play();
	}

	void DRAW(float timer)
	{
		timer /= 14500.0;

		r += asqrt(20 * units / 100 + 20 - r)*0.2; // ������ �� ������ ������

		if (mouse_enter())
		{
			if (jrad < r-4) jrad += sqrt(r-4 - jrad)*0.2*timer;
			if (jrad > r-4) jrad -= sqrt(jrad - (r-4))*0.2*timer;
		}
		else
		{
			if (jrad > r*0.6) jrad -= sqrt(jrad - r*0.6)*0.2*timer;
			if (jrad < r*0.6) jrad += sqrt(r*0.6 - jrad)*0.2*timer;
		}

		jrad = min(r, jrad);

		double ddir = sqrt(point_distance(mouse_x, mouse_y, view_x + start_x, view_y + start_y));
		double dalp = point_directionR(mouse_x, mouse_y, view_x + start_x, view_y + start_y);
		x_to = start_x - 15 * cos(dalp);
		y_to = start_y - 15 * sin(dalp);
		x -= asqrt(x - x_to)*0.3;
		y -= asqrt(y - y_to)*0.3;

		if (activate_id_from == id)
		{
			rect_sel.setSize(Vector2f(r*0.54, r*0.54));
			rect_sel.setPosition(Vector2f(x + view_x, y + view_y));
			rect_sel.setOrigin(Vector2f(0, 0));
			rect_sel.setRotation(point_directionG(view_x + x, view_y + y, mouse_x, mouse_y) - 45);
			window.draw(rect_sel);

			dir_sel++; dir_sel %= 360;
			for (int i = 0; i < 360; i++){
				int k = i + dir_sel;
				if (i % 120 < 60) {
					circle_sel.setPosition(Vector2f(view_x + x + cos(3.1415 / 180.0*(float)k + 0.05) * r, view_y + y + sin(3.1415 / 180.0*(float)k + 0.05) * r));
					window.draw(circle_sel);
				}
			}
		}

		draw_circleOut(view_x + x, view_y + y, r, 2, c_text, window);
		if (team == 0) col = c_gray;

		if (team == 1) col = c_green;
		if (team == 2) col = c_red;
		if (team == 3) col = c_blue;
		if (team == 4) col = c_yellow;

		draw_circle(view_x + x, view_y + y, jrad, col, window);
		draw_text_or(view_x + x - 9, view_y + y - 7, int_to_string(units,2), 8, c_background, c_text, window);
	}
};

class Panel
{
public:
	String txt = "";
	int left, top, height, width;
	int lr = 0, lg = 0;
	Panel(int l, int t, int w, int h)
	{
		left = l;
		top = t;
		height = h;
		width = w;
	}

	bool mouse_enter()
	{
		if (mouse_x >= left && mouse_y >= top && mouse_x <= left + width && mouse_y <= top + height) return true;
		return false;
	}

	void refresh(Node Nodes[])
	{
		team_count[4] = 0;
		team_count[1] = 0;
		team_count[2] = 0;
		team_count[3] = 0;
		for (int i = 0; i < nodes_count; i++)
			team_count[Nodes[i].team]++;
	}

	void EVENT(float time, Node Nodes[])
	{
		if (mouse_left_pressed && mouse_enter() && step <= 2){
			activate_id_to = -1;
			activate_id_from = -1;
			step++;
		}
	}

	void DRAW()
	{
		left = window_width - 80;
		top = 0;
		if (!((team_count[1] != 0 && team_count[2] == 0 && team_count[3] == 0 && team_count[4] == 0) || (team_count[1] == 0)))
		if (nodes_count > 0)
		{

			int l1 = (80.0 * (double)team_count[1] / (double)nodes_count);
			int l2 = (80.0 * (double)team_count[2] / (double)nodes_count);
			int l3 = (80.0 * (double)team_count[3] / (double)nodes_count);
			int l4 = (80.0 * (double)team_count[4] / (double)nodes_count);

			draw_rectangle(left, top, width, height, c_agrayD, window);

			draw_rectangle(left + 1, top + 69, l1, 10, c_green, window);
			draw_rectangle(left + 1 + l1, top + 69, l2, 10, c_red, window);
			draw_rectangle(left + 1 + l1 + l2, top + 69, l3, 10, c_blue, window);
			draw_rectangle(left + 1 + l1 + l2 + l3, top + 69, l4, 10, c_yellow, window);

			if (step == 0) txt = "> Add";
			if (step == 1) txt = "> Attack";
			if (step == 2) txt = "> Move";
			if (step == 3) txt = "> Enemy";
			draw_text_or(left + 2, top + 2, txt, 8, c_text, c_background, window);

			draw_text(left + 4, top + 19, "Click me", 8, c_text, window);
			draw_text(left + 8, top + 36, "to next", 8, c_text, window);
			draw_text(left + 22, top + 53, "step", 8, c_text, window);
		}
	}
};

class ButtonMenu
{
public:
	int left, top, height, width;
	bool open = false;
	double hh = 18;

	ButtonMenu(int l, int t, int w, int h)
	{
		left = l;
		top = t;
		height = h;
		width = w;
	}

	bool mouse_enter()
	{
		if (open)
		if (mouse_x >= left && mouse_y >= top && mouse_x <= left + width && mouse_y <= top + height) return true;
		if (!open)
		if (mouse_x >= left && mouse_y >= top && mouse_x <= left + width && mouse_y <= top + 20) return true;

		return false;
	}

	void EVENT()
	{
		if (mouse_left_pressed && mouse_enter()){
			room = 0;
			if (music_on) snd_back_game.stop();
			if (music_on) snd_back_menu.play();
		}
	}

	void DRAW(float timer)
	{
		timer /= 14500.0;
		if (mouse_enter()) { open = true; }
		else { open = false; }

		if (open) { if (hh < 26) hh += sqrt(26.0 - hh)*0.5*timer; }
		else { if (hh > 18) hh -= sqrt(hh - 18)*0.5*timer; }

		if (!((team_count[1] != 0 && team_count[2] == 0 && team_count[3] == 0 && team_count[4] == 0) || (team_count[1] == 0)))
		{
			draw_rectangle(0, 0, width, hh, c_agrayD, window);
			draw_text(left + 22, hh - 17, "Menu", 9, c_grayLL, window);
			width = 80;
			height = 30;
		}
		else {
			height = 80;
			width = window_width;
			draw_rectangle(0, 0, width, height, c_agrayD, window);
			if (team_count[1] == 0) draw_text(window_width / 2 - 31, 20, "You lose", 9, c_red, window);
			if (team_count[1] != 0 && team_count[2] == 0 && team_count[3] == 0 && team_count[4] == 0) draw_text(window_width / 2 - 31, 20, "You win", 9, c_red, window);
			open = true;
			draw_text(window_width / 2 - 126, 46, "Click here that exit to menu", 9, c_grayLL, window);
		}
	}
};

void FileDir(ListBox &LB, VScroll &VS)
{
	LB.Clear();
	string s;
	TCHAR szPath[MAX_PATH];
	DWORD cchPath = GetModuleFileName(0, szPath, MAX_PATH);
	if (cchPath)
	{
		while (cchPath && szPath[cchPath] != '\\')
			cchPath--;
		lstrcpy(&szPath[cchPath], _T("\\Maps\\*.map"));
		WIN32_FIND_DATA fd;
		HANDLE hf = FindFirstFile(szPath, &fd);
		if (hf != INVALID_HANDLE_VALUE)
		{
			do
			{
				if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
				{
					s = "";
					for (int i = 0; i < 200; i++)
					if (fd.cFileName[i] != '\0')
					{
						char ch1[1];
						WCHAR ch0[1];
						ch0[0] = fd.cFileName[i];
						setlocale(LC_CTYPE, "Russian_Russia.1251");
						wcstombs(ch1, ch0, 1);
						s = s + ch1[0];
					}
					else
						break;
					String ss = s;
					ss.erase(ss.getSize() - 4, 4);
					LB.ItemAdd(ss);
				}
			} while (FindNextFile(hf, &fd));
			FindClose(hf);
		}
	}
	if (LB.sizeOf != 0)
		VS.Heig = min(VS.height, max((LB.height*VS.height) / (LB.item_height*LB.sizeOf), 20));
	else { VS.Up = 0; VS.Heig = VS.height; }

	if (LB.item_height*LB.sizeOf <= LB.height) { VS.Up = 0; VS.Heig = VS.height; }
}

void function_attack(Node &Nfrom, Node &Nto, int count)
{
	count = min(count, Nfrom.units - 1);
	if (count > 0)
	{
		Nfrom.units -= count;
		while (Nto.units > 0 && count > 0)
		{
			if (rand() % 10 > 2) { Nto.units--; count--; }
			else if (rand() % 10 == 0) { count--; }
			else { Nto.units--; }	
		}

		if (Nto.units == 0 && count > 0)
		{
			Nto.units = count;
			Nto.team = Nfrom.team;
		}

		if (Nto.units == 0 && count == 0)
		{
			Nto.units = 1;
			Nto.team = Nfrom.team;
		}
	}	
}

int main(int argc, TCHAR* argv[], TCHAR* envp[])
{
	snd_back_menu.setLoop(true);
	snd_back_menu.openFromFile("Data\\back_menu.wav");

	snd_back_game.setLoop(true);
	snd_back_game.openFromFile("Data\\back_game.wav");

	snd_click.setLoop(false);
	snd_click.openFromFile("Data\\click.wav");

	snd_click2.setLoop(false);
	snd_click2.openFromFile("Data\\click2.wav");

	Texture tex_logo;
	tex_logo.loadFromFile("Data\\logo_menu.png");
	Sprite spr_logo;
	spr_logo.setTexture(tex_logo);

	texture_font.loadFromFile("Data\\font(9x15).png");
	snum.setTexture(texture_font);

	ContextSettings settings;
	settings.antialiasingLevel = 8;
	window.create(VideoMode::VideoMode(640,480), "Estratexia", Style::Titlebar, settings);
	window.setFramerateLimit(70);

	SYSTEMTIME systime;
	GetLocalTime(&systime);
	Clock clock;

	Texture texture_btn;
	texture_btn.loadFromFile("strip_menu.png");

	Label lab_author("", 0, 10, 5, 100, 15); // ������ ��������. �����. 12 ������.
	Button btn_play("Play", 0, 390, c_menuG);
	Button btn_options("Options", 0, 390, c_menuB);
	Button btn_exit("Exit", 0, 390, c_menuR);

	Label lab_select("Select map", 0, 0, 30, 640, 15);
	ListBox listbox_open(0, 56, 544, 320);
	VScroll vscroll(544, 56, 8, 320);
	Button btn_playgame("Start", 0, 0, c_menuG);
	Button btn_back1("Back", 0, 390, c_menuR);

	Label lab_sound("Sound", 0, 0, 60, 640, 15);
	CheckBox chb_music("music", 0, 80 + 27, 80, 16);
	CheckBox chb_effct("effects", 0, 110 + 27, 80, 16);
	Label lab_graphi("Graphics", 0, 180, 250 - 64, 640, 15);
	CheckBox chb_fullscr("full screen", 0, 227, 80, 16);
	CheckBox chb_vsunc("v-sync", 0, 227 + 30, 80, 16);
	Button btn_back2("Accept", 0, 390, c_menuG);

	Label lab_exit("Are You really want to exit?", 0, 180, 80, 640, 15);
	Button btn_exitgame("Yes", 0, 390, c_menuG);
	Button btn_back3("No", 0, 390, c_menuR);

	Panel pan_control(560, 0, 80, 80);

	ButtonMenu top_toMenu(0, 0, 80, 80);

	Node Nodes[1000];

	int EnemyNumber = 1;
	int EnemyAttackTry = 0;
	vector <int> EnemyAdd;
	vector <pair <int, int> > EnemyAttack;
	vector <pair <int, int> > EnemyMove;

	FileDir(listbox_open, vscroll);
	std::srand(time(NULL));
	while (window.isOpen())
	{
		float timer = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		mouse_x = Mouse::getPosition(window).x;
		mouse_y = Mouse::getPosition(window).y;
		window_width = window.getSize().x;
		window_height = window.getSize().y;

		switch (room)
		{
			//////////////////////////////////
			//				����			//	
		case 0:{
				   mousedelta = 0;
				   while (window.pollEvent(event))
				   {
					   if (mouse_left_pressed) { mouse_left = true; mouse_x_prev = mouse_x; mouse_y_prev = mouse_y; }
					   if (mouse_left_released) mouse_left = false;
					   if (mouse_right_pressed) mouse_right = true;
					   if (mouse_right_released) mouse_right = false;
					   if (mouse_middle_pressed) mouse_middle = true;
					   if (mouse_middle_released) mouse_middle = false;

					   if (event.type == Event::MouseWheelMoved && event.mouseWheel.delta != 0)
					   {
						   mousedelta = event.mouseWheel.delta;
						   event.mouseWheel.delta = 0;
					   }

					   if ((event.type == Event::KeyReleased && event.key.code == Keyboard::Escape) || (event.type == Event::Closed))
						   window.close();

					   switch (mode)
					   {
					   case 0://menu
						   btn_play.Update();
						   if (btn_play.press) { FileDir(listbox_open, vscroll); mode = 1; listbox_open.selectIndex = 0; listbox_open.offsetYto = 0; listbox_open.offsetY = 0; vscroll.Up = 0; btn_play.press = false; }
						   btn_options.Update();
						   if (btn_options.press) { mode = 2; btn_options.press = false; }
						   btn_exit.Update();
						   if (btn_exit.press) { mode = 3; btn_exit.press = false; }
						   break;
					   case 1://play
						   listbox_open.Update();
						   vscroll.Update(listbox_open);
						   btn_playgame.Update();
						   if (btn_playgame.press)
						   {
							   /* loading map */

							   activate_id_to = -1;
							   activate_id_from = -1;
							   for (int i = 0; i < 1000; i++) Nodes[i].Clean();

							   ifstream file("Maps\\" + (string)listbox_open.mass[listbox_open.selectIndex] + ".map");
							   file >> nodes_count;
							   for (int i = 0; i < nodes_count; i++)
							   for (int j = 0; j < nodes_count; j++)
								   file >> matrix[i][j];

							   for (int i = 0; i < nodes_count; i++)
							   {
								   int r_id, r_x, r_y;
								   file >> r_id;
								   file >> r_x;
								   file >> r_y;

								   r_x *= 1.5;
								   r_y *= 1.5;

								   Nodes[i].id = r_id;
								   Nodes[i].Set(r_x, r_y);
								   Nodes[i].units = 10 + rand() % 4 - 2;

								   if (i == 0) { view_bordier_left = view_bordier_right = r_x; view_bordier_top = view_bordier_bottom = r_y; }

								   view_bordier_left = min(view_bordier_left, Nodes[i].x);
								   view_bordier_right = max(view_bordier_right, Nodes[i].x);
								   view_bordier_top = min(view_bordier_top, Nodes[i].y);
								   view_bordier_bottom = max(view_bordier_bottom, Nodes[i].y);
							   }
							   file.close();

							   int adww = max(0, window_width - abs(view_bordier_left - view_bordier_right)) / 2 + 100;
							   int adhh = max(0, window_height - abs(view_bordier_top - view_bordier_bottom)) / 2 + 100;
							   view_bordier_left -= adww;
							   view_bordier_right += adww;
							   view_bordier_top -= adhh;
							   view_bordier_bottom += adhh;
							   view_x = -(view_bordier_right + view_bordier_left) / 2 + window_width / 2;
							   view_y = -(view_bordier_bottom + view_bordier_top) / 2 + window_height / 2;

							   // ������������� ������
							   int *distib = new int[nodes_count];
							   int sd = rand() % 4;
							   for (int k = 0; k < nodes_count; k++)
								   distib[k] = 1 + (k + sd) % 4;
							   for (int i = 0; i < nodes_count; ++i)
								   swap(distib[i], distib[rand() % nodes_count]);
							   for (int k = 0; k < nodes_count; k++)
								   Nodes[k].team = distib[k];

							   pan_control.refresh(Nodes);

							   step = -1;
							   room = 1;
							   btn_playgame.press = false;

							   if (music_on) snd_back_menu.stop();
							   if (music_on) snd_back_game.play();
						   }
						   btn_back1.Update();
						   if (btn_back1.press) { mode = 0; btn_back1.press = false; }
						   break;

					   case 2: // options
						   chb_effct.Update();
						   chb_music.Update();
						   chb_vsunc.Update();
						   chb_fullscr.Update();
						   btn_back2.Update();
						   if (btn_back2.press)
						   {
							   sound_on = chb_effct.check;
							   music_on = chb_music.check;
							   if (music_on) snd_back_menu.play();

							   if (chb_vsunc.check) {
								   window.setFramerateLimit(0);
								   window.setVerticalSyncEnabled(true);
							   }
							   else {
								   window.setVerticalSyncEnabled(false);
								   window.setFramerateLimit(70);
							   }

							   if ((window.getSize().x == 640 && chb_fullscr.check) || window.getSize().x == VideoMode::getDesktopMode().width && !chb_fullscr.check)
							   if (chb_fullscr.check) window.create(VideoMode::getDesktopMode(), "Estratexia", Style::Fullscreen, settings);
							   else window.create(VideoMode(640, 480), "Estratexia", Style::Titlebar, settings);

							   mode = 0; btn_back2.press = false;
						   }
						   break;
					   case 3:
						   btn_exitgame.Update();
						   if (btn_exitgame.press) { window.close(); btn_exitgame.press = false; }
						   btn_back3.Update();
						   if (btn_back3.press) { mode = 0; btn_back3.press = false; }
						   break;
					   }
				   }

				   // ������ ����
				   spr_logo.setPosition((window_width - 402) / 2, (window_height - 100 - 85) / 2);
				   lab_author.setPosition(5, 5);
				   btn_options.setPosition(window_width / 4 * 1, window_height - 50);
				   btn_play.setPosition(window_width / 4 * 2, window_height - 50);
				   btn_exit.setPosition(window_width / 4 * 3, window_height - 50);

				   lab_select.setPosition(40, 25);
				   listbox_open.setPosition(40, 40);
				   listbox_open.setSize(window_width - 80 - 16, window_height - 140 - 8);
				   vscroll.setPosition(window_width - 40 - 10, 40);
				   vscroll.setSize(10, window_height - 140 - 8);
				   btn_playgame.setPosition(window_width / 3 * 1, window_height - 50);
				   btn_back1.setPosition(window_width / 3 * 2, window_height - 50);

				   lab_sound.setPosition(window_width / 2 - 60, (window_height - 100) / 2 - 108 + 27 * 0);
				   chb_effct.setPosition(window_width / 2 - 60, (window_height - 100) / 2 - 108 + 27 * 1);
				   chb_music.setPosition(window_width / 2 - 60, (window_height - 100) / 2 - 108 + 27 * 2);
				   lab_graphi.setPosition(window_width / 2 - 60, (window_height - 100) / 2 - 108 + 27 * 4);
				   chb_vsunc.setPosition(window_width / 2 - 60, (window_height - 100) / 2 - 108 + 27 * 5);
				   chb_fullscr.setPosition(window_width / 2 - 60, (window_height - 100) / 2 - 108 + 27 * 6);
				   btn_back2.setPosition(window_width / 2, window_height - 50);

				   lab_exit.setPosition(window_width / 2 - 125, (window_height - 100) / 2);
				   btn_exitgame.setPosition(window_width / 3 * 1, window_height - 50);
				   btn_back3.setPosition(window_width / 3 * 2, window_height - 50);

				   window.clear(c_menu);

				   draw_rectangle(40, window_height - 100, window_width - 80, 1, c_grayLL, window);


				   switch (mode)
				   {
				   case 0:
					   window.draw(spr_logo);
					   lab_author.Draw();
					   btn_play.Draw(timer);
					   btn_options.Draw(timer);
					   btn_exit.Draw(timer);
					   break;
				   case 1:
					   lab_select.Draw();
					   listbox_open.Draw();
					   vscroll.Draw(listbox_open);
					   btn_playgame.Draw(timer);
					   btn_back1.Draw(timer);
					   break;
				   case 2:
					   lab_sound.Draw();
					   chb_effct.Draw(timer);
					   chb_music.Draw(timer);
					   chb_vsunc.Draw(timer);
					   lab_graphi.Draw();
					   chb_fullscr.Draw(timer);
					   btn_back2.Draw(timer);
					   break;
				   case 3:
					   lab_exit.Draw();
					   btn_exitgame.Draw(timer);
					   btn_back3.Draw(timer);
					   break;
				   }

				   window.display();
		} break;
			//////////////////////////////////
			//				����		    //	
		case 1:{
				   mousedelta = 0;
				   while (window.pollEvent(event))
				   {
					   if (mouse_left_pressed) { mouse_left = true; mouse_x_prev = mouse_x; mouse_y_prev = mouse_y; }
					   if (mouse_left_released) mouse_left = false;
					   if (mouse_right_pressed) mouse_right = true;
					   if (mouse_right_released) mouse_right = false;
					   if (mouse_middle_pressed) mouse_middle = true;
					   if (mouse_middle_released) mouse_middle = false;

					   if (event.type == Event::MouseWheelMoved && event.mouseWheel.delta != 0)
					   {
						   mousedelta = event.mouseWheel.delta;
						   event.mouseWheel.delta = 0;
					   }

					   if ((event.type == Event::KeyReleased && event.key.code == Keyboard::Escape) || (event.type == Event::Closed))
						   window.close();

					   if (mouse_y_prev < 80 && mouse_x_prev > window_width - 80)
					   {
						   if (mouse_left_released) { mouse_x_prev = 0; }
					   }
					   else
					   {
						   // ����������� �� �����
						   if (mouse_middle_pressed || mouse_right_pressed)
						   {
							   vm_x = mouse_x - view_x;
							   vm_y = mouse_y - view_y;
						   }
						   if (mouse_middle || mouse_right)
						   {
							   view_x = mouse_x - vm_x;
							   view_y = mouse_y - vm_y;
							   if (-view_x < view_bordier_left) view_x = -view_bordier_left;
							   if (-view_x > view_bordier_right - window_width) view_x = -view_bordier_right + window_width;
							   if (-view_y < view_bordier_top) view_y = -view_bordier_top;
							   if (-view_y > view_bordier_bottom - window_height) view_y = -view_bordier_bottom + window_height;
						   }

						   for (int i = 0; i < nodes_count; i++)
							   Nodes[i].EVENT(timer, Nodes);
					   }

					   top_toMenu.EVENT();
					   pan_control.EVENT(timer, Nodes);
				   }// end event

				   if (step == -1)
				   {
					   pan_control.refresh(Nodes);

					   for (int k = 1; k <= 4; k++)
						   add_unit[k] = team_count[k]*5;

					   step = 0;
				   }

				   // player add
				   if (step == 0 && add_unit[1] == 0) step = 1;

				   // player attack
				   if (step == 1 && activate_id_from != -1 && activate_id_to != -1)
				   {
					   function_attack(Nodes[activate_id_from], Nodes[activate_id_to], Nodes[activate_id_from].send);

					   pan_control.refresh(Nodes);
					   activate_id_from = -1;
					   activate_id_to = -1;
				   }

				   
				   // player move
				   if (step == 2 && activate_id_from != -1 && activate_id_to != -1 && nextstep)
				   {
					   int send = Nodes[activate_id_from].send;

					   Nodes[activate_id_from].units -= send;
					   Nodes[activate_id_to].units += send;

					   pan_control.refresh(Nodes);
					   activate_id_from = -1;
					   activate_id_to = -1;
					   nextstep = false; // ��� �� ������ ������ �� ������
				   }


				   if (step > 2) // ��� ����������
				   {
					   nextstep = true;
					  
					   for (int enemy = 2; enemy <= 4; enemy++)
					   {
						   // enemy add

						   pan_control.refresh(Nodes);

						   vector <pair <int, int>> enemy_map; // ������ �������������

						   for (int i = 0; i < nodes_count; i++) // ������� ��� ���� � ������� ����������� ���������� �������
						   if (Nodes[i].team == enemy)
						   {
							   int neob_max = 0;
							   for (int ju = 0; ju < nodes_count; ju++)
							        if (connect_get(i, ju) && Nodes[ju].team != enemy)
								         neob_max += Nodes[ju].units + 5;
							   neob_max = max(0, neob_max - Nodes[i].units);
							   if (neob_max > 0) enemy_map.push_back(make_pair(neob_max, i));
						   }

						   std::sort(enemy_map.begin(), enemy_map.end());
						   std::reverse(enemy_map.begin(), enemy_map.end());

						   if (enemy_map.size() > 0)
						   {
							   for (int i = 0; i < enemy_map.size(); i++){
								   int g = min(enemy_map[i].first, add_unit[enemy]);
								   Nodes[enemy_map[i].second].units += g;
								   add_unit[enemy] -= g;
							   }

							   while (add_unit[enemy] > 0)
								   for (int i = 0; i < enemy_map.size(); i++){
									   int g = min(add_unit[enemy] / 2 + 1, add_unit[enemy]);
									   Nodes[enemy_map[i].second].units += g;
									   add_unit[enemy] -= g;
								   }


							   // enemy attack

								   for (int repeat = 0; repeat < 8; repeat++)
								   for (int ip = 0; ip < nodes_count; ip++)
								   if (Nodes[ip].team == enemy && Nodes[ip].units>1)
								   {
									   vector <pair <int, int>> attack_map;

									   for (int j = 0; j < nodes_count; j++)
									   if (Nodes[j].team != enemy && connect_get(ip, j))
									   if (Nodes[j].units - Nodes[ip].units<0) attack_map.push_back(make_pair(Nodes[j].units - Nodes[ip].units, j));

									   if (attack_map.size() > 0)
									   {
										   std::sort(attack_map.begin(), attack_map.end());
										   for (int j = 0; j < attack_map.size(); j++)
											   function_attack(Nodes[ip], Nodes[attack_map[j].second], Nodes[attack_map[j].second].units*1.5);
									   }
								   }

							   // enemy move

							   vector <pair <int, int> > perenos_to;
							   vector <pair <int, int> > perenos_from;

							   for (int ip = 0; ip < nodes_count; ip++)
							   {
								   if (Nodes[ip].team == enemy)
								   {
									   int nak = 0;
									   for (int j = 0; j < nodes_count; j++)
									   if (Nodes[j].team != enemy && connect_get(ip, j))
										   nak += 1 + Nodes[j].units;

									   if (nak == 0) perenos_from.push_back(make_pair(Nodes[ip].units, ip));
									   else perenos_to.push_back(make_pair(min(0, nak - Nodes[ip].units) , ip));
								   }
							   }
							   
							   std::sort(perenos_from.begin(), perenos_from.end());
							   std::reverse(perenos_from.begin(), perenos_from.end());
							   std::sort(perenos_to.begin(), perenos_to.end());
							   std::reverse(perenos_to.begin(), perenos_to.end());

							   if (perenos_from.size() > 0 && perenos_to.size() > 0)
							   {
								   int g = max(0, Nodes[perenos_from[0].second].units - 1);
								   Nodes[perenos_from[0].second].units -= g;
								   Nodes[perenos_to[0].second].units += g;
							   }

						   }

					   } // all enemy end

					   step = -1; // �� ��� � ���������
				   }

				   // DRAW //
				   window.clear(c_menu);

				   for (int i = 0; i < nodes_count; i++)
					   Nodes[i].DRAW(timer);

				   for (int i = 0; i < nodes_count - 1; i++) // ������ ����� �������
				   for (int j = i + 1; j < nodes_count; j++)
				   if (connect_get(i, j))
					   draw_lineW((float)Nodes[i].x + view_x, (float)Nodes[i].y + view_y, (float)Nodes[j].x + view_x, (float)Nodes[j].y + view_y, 2, Nodes[i].r - 2, Nodes[j].r - 2, c_text, window);

				   if (activate_id_from != -1) // ������ � ���������
				   {
					   draw_rectangle(mouse_x, mouse_y, 18, 18, c_green, window);
					   draw_circle(18 + mouse_x, 18 + mouse_y, 18, c_green, window);
					   draw_text_or(18 + mouse_x - 9, 18 + mouse_y - 7, int_to_string(Nodes[activate_id_from].send, 2), 8, c_background, c_text, window);
				   }

				   if (add_unit[1] > 0 && step == 0) // ������ � ��������
				   {
					   draw_rectangle(mouse_x, mouse_y, 18, 18, c_background, window);
					   draw_circle(18 + mouse_x, 18 + mouse_y, 18, c_background, window);
					   draw_text(18 + mouse_x - 9, 18 + mouse_y - 7, int_to_string(add_unit[1], 2) , 8, c_text, window);
				   }

				   //���������
				   pan_control.DRAW();
				   top_toMenu.DRAW(timer);

				   window.display();
		} break;
		default:
			break;
		}
		GetLocalTime(&systime);
	}
	return 0;
}